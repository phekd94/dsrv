
/*
DESCRITION: class for storing data
TODO:
 * add random data and size to the tests
FIXME:
DANGER:
NOTE:

Thread safety: YES, but NO for the move constructor
Reentrance: YES, but take into account that only two buffers are available
*/

#include "gtest/gtest.h"

#include <vector>   // vector
#include <utility>  // move

#include "DSrv/L1/Storage_2_buffers.h"  // Storage_2_buffers
#include "DSrv/other/Exception.h"       // Exception

using namespace DSrv;

//-------------------------------------------------------------------------------------------------
TEST(Storage_2_buffers, memberPointers)
{
	Storage_2_buffers obj;

	// Test all methods which utilize data pointers
	ASSERT_THROW(obj.setData(Storage_2_buffers::Data_t()), Exception);
	ASSERT_THROW(obj.getData(), Exception);
	ASSERT_THROW(obj.completeData(), Exception);

	// Test all methods which utilize input pointers
	obj.allocate(32);
	ASSERT_THROW(obj.setData(Storage_2_buffers::Data_t(nullptr, 0)), Exception);

	// Re-allocation
	ASSERT_THROW(obj.allocate(32), Exception);

	// Copy and assign without allocation
	Storage_2_buffers obj_1;
	ASSERT_THROW(Storage_2_buffers obj_2(obj_1), Exception);
	Storage_2_buffers obj_2;
	ASSERT_THROW(obj_2 = obj_1, Exception);
}

//-------------------------------------------------------------------------------------------------
TEST(Storage_2_buffers, data)
{
	std::vector<uint8_t> data_1 {1};
	std::vector<uint8_t> data_2 {3};
	std::vector<uint8_t> data_1_2(data_1);
	data_1_2.insert(data_1_2.end(), data_2.begin(), data_2.end());
	Storage_2_buffers::Data_t dataGet;

	Storage_2_buffers obj;
	obj.allocate(data_1.size() + data_2.size());

	obj.setData(Storage_2_buffers::Data_t(data_1.data(), data_1.size()));
	obj.setData(Storage_2_buffers::Data_t(data_2.data(), data_2.size()));
	dataGet = obj.getData();
	ASSERT_EQ(dataGet.second, 0);
	obj.completeData();
	dataGet = obj.getData();
	ASSERT_TRUE(std::vector<uint8_t>(dataGet.first, dataGet.first + dataGet.second) == data_1_2);
}

//-------------------------------------------------------------------------------------------------
TEST(Storage_2_buffers, maxSize)
{
	std::vector<uint8_t> data {1, 2};

	Storage_2_buffers obj;
	obj.allocate(data.size() * 2 - 1);

	obj.setData(Storage_2_buffers::Data_t(data.data(), data.size()));
	ASSERT_THROW(obj.setData(Storage_2_buffers::Data_t(data.data(), data.size())), Exception);
}

//-------------------------------------------------------------------------------------------------
TEST(Storage_2_buffers, copyConstructor)
{
	std::vector<uint8_t> data {1, 2};
	Storage_2_buffers::Data_t dataGet;

	Storage_2_buffers obj_1;
	obj_1.allocate(data.size());
	obj_1.setData(Storage_2_buffers::Data_t(data.data(), data.size()));

	Storage_2_buffers obj_2(obj_1);

	obj_1.completeData();
	dataGet = obj_1.getData();
	ASSERT_TRUE(std::vector<uint8_t>(dataGet.first, dataGet.first + dataGet.second) == data);

	obj_2.completeData();
	dataGet = obj_2.getData();
	ASSERT_TRUE(std::vector<uint8_t>(dataGet.first, dataGet.first + dataGet.second) == data);
}

//-------------------------------------------------------------------------------------------------
TEST(Storage_2_buffers, opEqual)
{
	std::vector<uint8_t> data_1 {1, 2};
	std::vector<uint8_t> data_2 {3};
	Storage_2_buffers::Data_t dataGet;

	Storage_2_buffers obj_1;
	obj_1.allocate(data_1.size());
	obj_1.setData(Storage_2_buffers::Data_t(data_1.data(), data_1.size()));
	Storage_2_buffers obj_2;
	obj_2.allocate(data_2.size());
	obj_2.setData(Storage_2_buffers::Data_t(data_2.data(), data_2.size()));

	obj_1 = obj_1;
	obj_2 = obj_1;

	obj_2.completeData();
	dataGet = obj_2.getData();
	ASSERT_TRUE(std::vector<uint8_t>(dataGet.first, dataGet.first + dataGet.second) == data_1);
}

//-------------------------------------------------------------------------------------------------
TEST(Storage_2_buffers, moveConstructor)
{
	std::vector<uint8_t> data {1, 2};
	Storage_2_buffers::Data_t dataGet;

	Storage_2_buffers obj_1;
	obj_1.allocate(data.size());
	obj_1.setData(Storage_2_buffers::Data_t(data.data(), data.size()));

	Storage_2_buffers obj_2 {std::move(obj_1)};

	ASSERT_THROW(obj_1.completeData(), Exception);
	obj_2.completeData();
	dataGet = obj_2.getData();
	ASSERT_TRUE(std::vector<uint8_t>(dataGet.first, dataGet.first + dataGet.second) == data);
}
