
#pragma once

/*
DESCRITION: exception class
TODO:
 * Add numbers for exceptions
FIXME:
DANGER:
NOTE:
*/

#include <stdexcept>  // runtime_error
#include <string>     // string

namespace DSrv
{

class Exception : public std::runtime_error
{
public:
	Exception(const std::string & s) : std::runtime_error(s) {}
};

} // namespace DSrv
