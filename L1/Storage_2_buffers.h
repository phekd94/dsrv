
#pragma once

/*
DESCRITION: class for storing data
TODO:
 * m_streambuf.setPointers() does not run when error is occured
 * mutex test
FIXME:
DANGER:
NOTE:

Thread safety: YES, but NO for the move constructor
Reentrance: YES, but take into account that only two buffers are available
*/

#include <cstdint>
#include <istream>
#include <memory>
#include <mutex>
#include <streambuf>
#include <utility>

namespace DSrv
{

class Storage_2_buffers
{

public:

	// Data type for set and get methods (pointer + size)
	typedef  std::pair<const uint8_t *, uint32_t>  Data_t;

	Storage_2_buffers();
	virtual ~Storage_2_buffers();
	Storage_2_buffers(const Storage_2_buffers & obj);
	Storage_2_buffers(Storage_2_buffers && obj);
	Storage_2_buffers & operator=(const Storage_2_buffers & obj);

	// Allocates memory for buffers
	void allocate(const uint32_t size);

	// Deallocates memory for buffers
	void deallocate() noexcept;

	// Sets data in the storage
	void setData(const Data_t & data);

	// Gets complete data from the storage
	Data_t getData();

	// Deletes all data in the storage
	void clearData();

	// Sets filling data as complete
	void completeData();

	// Gets pointer to istream object
	std::istream * getIstreamPointer() noexcept
	{
		return &m_istream;
	}

	// Applies clear() method to the std::istream object
	void clearIstream();

	// Enables debug messages
	void setDebug(const bool d) noexcept
	{
		m_debug = d;
	}

private:

	// Buffer class
	class Streambuf : public std::streambuf
	{

	public:

		// Default constructor
		Streambuf() = default;

		// Move constructor
		Streambuf(Streambuf && obj);

		// Sets the pointers 
		void setPointers(char * begin, char * curr, char * end) noexcept
		{
			setg(begin, curr, end);
		}

	} m_streambuf;

	// Input stream
	std::istream m_istream;

	// Pointers to the data
	std::unique_ptr<uint8_t []> m_completeData { nullptr };
	std::unique_ptr<uint8_t []> m_fillingData { nullptr };

	// Data size
	const uint32_t m_dataSize { 0 };

	// Index in the array of filling data
	uint32_t m_fillingIndex { 0 };

	// Size of the complete data
	uint32_t m_completeSize { 0 };

	// Mutex
	std::mutex m_mutex;

	// Enable debug messages
	bool m_debug { false };
};

} // namespace DSrv
