
#include "Storage_2_buffers.h"

#include <algorithm>
#include <cstring>
#include <exception>
#include <utility>

#include "DSrv/other/Exception.h"
#include "DSrv/other/printDebug.h"

using namespace DSrv;

//-------------------------------------------------------------------------------------------------
Storage_2_buffers::Streambuf::Streambuf(Storage_2_buffers::Streambuf && obj)
{
	// Set pointer from moving object
	setPointers(obj.eback(), obj.gptr(), obj.egptr());

	PRINT_DBG(true, "Move constructor");
}

//-------------------------------------------------------------------------------------------------
Storage_2_buffers::Storage_2_buffers() : m_istream(&m_streambuf)
{
	PRINT_DBG(m_debug, "");
}

//-------------------------------------------------------------------------------------------------
Storage_2_buffers::~Storage_2_buffers()
{
	PRINT_DBG(m_debug, "");
}

//-------------------------------------------------------------------------------------------------
Storage_2_buffers::Storage_2_buffers(const Storage_2_buffers & obj) : m_istream(&m_streambuf)
{
	// Lock a mutex being copied
	std::lock_guard<std::mutex> lock {const_cast<Storage_2_buffers &>(obj).m_mutex};

	// Check the new buffer memory
	if (obj.m_completeData.get() == nullptr || obj.m_fillingData.get() == nullptr)
		throw Exception("Bad new buffer memory");

	// Set buffers parameters
	*const_cast<uint32_t *>(&m_dataSize) = obj.m_dataSize;
	m_fillingIndex = obj.m_fillingIndex;
	m_completeSize = obj.m_completeSize;
	m_debug = obj.m_debug;

	// Allocate a memory for the data
	allocate(m_dataSize);

	// Copy data (+ 1 for last element)
	std::copy(obj.m_completeData.get(), obj.m_completeData.get() + m_dataSize + 1,
	          m_completeData.get());
	std::copy(obj.m_fillingData.get(), obj.m_fillingData.get() + m_dataSize + 1,
	          m_fillingData.get());

	// Set pointers to the input buffer
	m_streambuf.setPointers(reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get() + m_fillingIndex));

	PRINT_DBG(m_debug, "Copy constructor");
}

//-------------------------------------------------------------------------------------------------
Storage_2_buffers::Storage_2_buffers(Storage_2_buffers && obj) 
	: m_streambuf(std::move(obj.m_streambuf)), 
	  m_istream(&m_streambuf), 
	  m_completeData(std::move(obj.m_completeData)),
	  m_fillingData(std::move(obj.m_fillingData)),
	  m_dataSize(obj.m_dataSize),
	  m_fillingIndex(obj.m_fillingIndex),
	  m_completeSize(obj.m_completeSize),
	  m_debug(obj.m_debug)
{
	PRINT_DBG(m_debug, "Move constructor");
}

//-------------------------------------------------------------------------------------------------
Storage_2_buffers & Storage_2_buffers::operator=(const Storage_2_buffers & obj)
{
	// Self-assignment check
	if (&obj == this)
	{
		PRINT_DBG(m_debug, "Storage_2_buffers: Self-assignment");
		return *this;
	}

	// Lock mutexes
	std::lock_guard<std::mutex> lock { m_mutex };
	std::lock_guard<std::mutex> lock_obj { const_cast<Storage_2_buffers &>(obj).m_mutex };

	// Check the new buffer memory
	if (obj.m_completeData.get() == nullptr || obj.m_fillingData.get() == nullptr)
		throw Exception("Bad new buffer memory");

	// Data sizes are not match
	if (obj.m_dataSize != m_dataSize)
	{
		PRINT_DBG(m_debug, "Delete old data and allocate memory for new data");

		// Delete old buffers
		deallocate();

		// New data size member
		*const_cast<uint32_t *>(&m_dataSize) = obj.m_dataSize;

		allocate(m_dataSize);
	}

	// New buffer parameters
	m_fillingIndex = obj.m_fillingIndex;
	m_completeSize = obj.m_completeSize;
	m_debug = obj.m_debug;

	// Copy data (+ 1 for last element)
	std::copy(obj.m_completeData.get(), obj.m_completeData.get() + m_dataSize + 1,
	          m_completeData.get());
	std::copy(obj.m_fillingData.get(), obj.m_fillingData.get() + m_dataSize + 1,
	          m_fillingData.get());

	// Set pointers to the input buffer
	m_streambuf.setPointers(reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get() + m_fillingIndex));

	PRINT_DBG(m_debug, "Storage_2_buffers");

	return *this;
}

//-------------------------------------------------------------------------------------------------
void Storage_2_buffers::allocate(const uint32_t size)
{
	// Check the data pointers
	if (nullptr != m_completeData.get() || nullptr != m_fillingData.get())
		throw Exception("Memory for data already allocated");

	// Allocate a memory for the data (+ 1 for element for end pointer position of the Streambuf)
	m_completeData = std::unique_ptr<uint8_t []>(new uint8_t[size + 1]);
	m_fillingData = std::unique_ptr<uint8_t []>(new uint8_t[size + 1]);

	// Set pointers to the input buffer
	m_streambuf.setPointers(reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get()));

	// Set data size member
	*const_cast<uint32_t *>(&m_dataSize) = size;

	PRINT_DBG(m_debug, "");
}

//-------------------------------------------------------------------------------------------------
void Storage_2_buffers::deallocate() noexcept
{
	// Delete data
	delete [] m_completeData.release();
	delete [] m_fillingData.release();

	PRINT_DBG(m_debug, "");
}

//-------------------------------------------------------------------------------------------------
void Storage_2_buffers::setData(const Data_t & data)
{
	// Check the data pointers
	if (nullptr == m_completeData.get() || nullptr == m_fillingData.get())
		throw Exception("Memory for data have not been allocated");

	// Check the incoming parameter
	if (nullptr == data.first)
		throw Exception("nullptr == data.first");

	// Lock a mutex
	std::lock_guard<std::mutex> lock { m_mutex };

	// Check the size of the input data
	if (m_fillingIndex + data.second > m_dataSize)
		throw Exception("Size of the data is too much (size = " + std::to_string(data.second) +
	                    ", fillingIndex = " + std::to_string(m_fillingIndex) + ")");

	PRINT_DBG(m_debug, "Add the data(0x%p) with size(%5lu) to the 0x%p with size(%5lu)",
	                   data.first,
	                   static_cast<unsigned long>(data.second),
	                   m_fillingData.get(),
	                   static_cast<unsigned long>(m_fillingIndex));

	// Add the data and add the size to the fillingIndex
	std::memcpy(m_fillingData.get() + m_fillingIndex, data.first, data.second);
	m_fillingIndex += data.second;

	// Set pointers to the input buffer
	m_streambuf.setPointers(reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get() + m_fillingIndex));
}

//-------------------------------------------------------------------------------------------------
Storage_2_buffers::Data_t Storage_2_buffers::getData()
{
	// Check the data pointers
	if (nullptr == m_completeData.get() || nullptr == m_fillingData.get())
		throw Exception("Memory for data have not been allocated");

	// Lock a mutex
	std::lock_guard<std::mutex> lock { m_mutex };

	PRINT_DBG(m_debug, "Data for geter: 0x%p with size(%5lu)",
	                   m_completeData.get(),
	                   static_cast<unsigned long>(m_completeSize));

	// Return the data and the size
	return Data_t(m_completeData.get(), m_completeSize);
}

//-------------------------------------------------------------------------------------------------
void Storage_2_buffers::clearData()
{
	// Lock a mutex
	std::lock_guard<std::mutex> lock { m_mutex };

	// Set the fillingIndex and the completeSize to 0
	m_fillingIndex = 0;
	m_completeSize = 0;

	// Set pointers to the input buffer
	m_streambuf.setPointers(reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get()));

	PRINT_DBG(m_debug, "Data is clear");
}

//-------------------------------------------------------------------------------------------------
void Storage_2_buffers::completeData()
{
	// Check the data pointers
	if (nullptr == m_completeData.get() || nullptr == m_fillingData.get())
		throw Exception("Memory for data have not been allocated");

	// Lock a mutex
	std::lock_guard<std::mutex> lock { m_mutex };

	// Exchange pointers
	std::swap(m_fillingData, m_completeData);

	// Set the completeSize
	m_completeSize = m_fillingIndex;

	// Set the fillingIndex to 0
	m_fillingIndex = 0;

	// Set pointers to the input buffer
	m_streambuf.setPointers(reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get()),
	                        reinterpret_cast<char *>(m_fillingData.get()));

	PRINT_DBG(m_debug, "Data(0x%p) with size(%5lu) is complete",
	                   m_completeData.get(),
	                   static_cast<unsigned long>(m_completeSize));
}

//-------------------------------------------------------------------------------------------------
void Storage_2_buffers::clearIstream()
{
	m_istream.clear();
}
